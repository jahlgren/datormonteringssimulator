﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trigger : MonoBehaviour {

	public static List<Trigger> triggers =  new List<Trigger>();
	
	private Test _test;
	public Test test
	{
		get
		{
			return _test;
		}
		set
		{
			_test = value;
			
			if(_test == null) collider.enabled = true;
			else collider.enabled = false;
		}
	}
	
	public Quaternion triggerRotation;
	
	void Start ()
	{
		test = null; //GameObject.FindGameObjectWithTag("Player").GetComponent<Test>();
		gameObject.renderer.enabled = false;
		triggerRotation = transform.rotation;
		triggers.Add (this);
		
	}
	
	void Update()
	{
		if(Test.isEnabled && _test == null) gameObject.renderer.enabled = true;
		else gameObject.renderer.enabled = false;
		/*
		if(Test.isEnabled == false) gameObject.collider.enabled = false;
		else gameObject.collider.enabled = true;
		*/
		
	}
	
	void OnDestroy()
	{
		triggers.Remove(this);
	}
	
}
