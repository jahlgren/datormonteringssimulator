﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Test : MonoBehaviour {

	public static bool isEnabled = false;

	public bool mouseDown = false;
	public bool dragging = false;
	
	private Vector3 startPos;
	//private float dist;
	
	private Vector3 targetPos;
	
	public LayerMask _default;
	
	private Collider computerNormalCollider;
	
	int layerMask = 1 << 8;
	Vector3 snapPos;
	Vector3 offset;
	float sqrLen;
	
	public bool isSnapped = false;
	
	public float snapDistance;
	
	private Trigger _trigger;
	public Trigger trigger
	{
		 get
		 {
		 	return _trigger;
		 }
		 set
		{	
			if(_trigger != null)
				_trigger.test = null;
				
		 	_trigger = value;
		 	
		 	if(_trigger != null)
		 		_trigger.test = this;
		 }
		 
	}
	
	List<Trigger> subTriggers = new List<Trigger>();
	
	Quaternion rotation;
	Quaternion startRot;
	float rotationSpeed = 15f;
	
	private void Start()
	{
		
		startRot= rotation = transform.rotation;
		startPos = targetPos = transform.position;
		_default = gameObject.layer;
		computerNormalCollider = GameObject.Find("Shit").GetComponent<MeshCollider>();
		
		foreach(Transform child in transform)
		{
			if(child.GetComponent<Trigger>() != null)
			{
				subTriggers.Add (child.GetComponent<Trigger>());
			}
		}
	}
	
	
	void Update()
	{
		if(dragging)
		{
			Vector3 tmp = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2));
			targetPos = tmp;
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			RaycastHit hit;
			
			bool skip = false;
			
			for(int i = 0; i < Trigger.triggers.Count; i++)
			{
				if(!ContainsTrigger(Trigger.triggers[i]) && Trigger.triggers[i].collider.Raycast(ray, out hit, 100f))
				{
					print("hej");
					targetPos = hit.transform.position;
					snapPos = hit.transform.position;
					skip = true;
					rotation = Trigger.triggers[i].transform.rotation;
					break;
				}
			}
			
			if(skip == false && computerNormalCollider.Raycast(ray, out hit, 100f))
			{
				if(hit.transform.name == "Shit")
				{
					targetPos = hit.point;
					sqrLen = 0f;
				}
			}
		}
		
		Vector3 tmp2 = transform.position;
		tmp2 += (targetPos - tmp2) * Time.deltaTime*16;
		transform.position = tmp2;
		
		if(_trigger == null || Test.isEnabled == false)
		{
			foreach(Transform child in transform)
			{
				child.gameObject.renderer.enabled = false;
				child.gameObject.collider.enabled = false;
			}
		}
		else
		{
			foreach(Transform child in transform)
			{
				child.gameObject.renderer.enabled = true;
				child.gameObject.collider.enabled = true;
			}
		}
		
		transform.rotation = Quaternion.Lerp(transform.rotation,rotation,Time.deltaTime * rotationSpeed);
	}
	
	void OnMouseDown()
	{
		for(int i = 0; i < subTriggers.Count; i++)
		{
			if(subTriggers[i].test != null)
			{
				return;
			}
		}
		
		mouseDown = true;
		trigger = null;
	}
	
	void OnMouseExit()
	{
		if(mouseDown)
		{
			dragging = true;
			sqrLen = 0f;
			isEnabled = true;
		}
		
	}
	
	void OnMouseUp()
	{
		for(int i = 0; i < subTriggers.Count; i++)
		{
			if(subTriggers[i].test != null)
			{
				return;
			}
		}
	
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		RaycastHit hit;
		
		bool triggerFound = false;
		
		for(int i = 0; i < Trigger.triggers.Count; i++)
		{
			if(Trigger.triggers[i].collider.Raycast(ray, out hit, 100f))
			{
				transform.position = hit.transform.position;
				trigger = Trigger.triggers[i];
				triggerFound = true;
				break;
			}
		}
		
		dragging = false;
		mouseDown = false;
		isEnabled = false;
		
		if(!triggerFound){
			rotation = startRot;
			targetPos = startPos;
		}
		
		if(_trigger != null && transform.childCount != 0)
		{
			foreach(Transform child in transform)
			{
				child.gameObject.renderer.enabled = true;
				child.gameObject.collider.enabled = true;
			}
		}
		
	}
	
	void OnMouseOver()
	{
		if(isEnabled == false && _trigger == null)
		{
			Vector3 hover = transform.position + new Vector3(0,Random.Range(0.8f,1.4f),0);
			
			if(hover.y - 1f > startPos.y) hover.y -= 0.5f;
			
			transform.position = Vector3.Lerp(transform.position,hover,Time.deltaTime * 0.5f);
		}
	}
	
	bool ContainsTrigger(Trigger _triggger)
	{
		for(int i = 0; i < subTriggers.Count; i++)
		{
			if(subTriggers[i] == _triggger) return true;
		}
		return false;
	}
	
}
