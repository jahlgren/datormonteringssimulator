﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComputerComponent : MonoBehaviour {

    public static List<ComputerComponent> components = new List<ComputerComponent>();

	public static ComputerComponent hoverObject;

	public static bool isEnabled = false;
	
	public static string componentName;
	
	public Trigger.SlotTypes targetSlotType;
	
	public bool isHovering = false;

	public bool mouseDown = false;
	public bool dragging = false;
	
	private Vector3 startPos;
	
	private Vector3 targetPos;
	
	private Collider computerNormalCollider;
	private Vector3 offset;
	
	public bool isSnapped = false;
	
	private Trigger _lastTrigger;
	private Trigger _trigger;
	public Trigger trigger
	{
		get
		{
			return _trigger;
		}
		set
		{
			if(_trigger != null)
			{
				_trigger.computerComponent = null;
			}
			
		 	_trigger = value;
		 	
		 	if(_trigger != null)
		 	{
				_trigger.computerComponent = this;
		 	}
		}
	}
	
	List<Trigger> subTriggers = new List<Trigger>();
	
	Quaternion rotation;
	Quaternion startRot;
	float rotationSpeed = 15f;

	private float hoverTime = 0;

	private BoxCollider hoverExtraCollider;

    void Awake()
    {
        components.Add(this);
    }

	void Start()
	{
		startRot= rotation = transform.rotation;
		startPos = targetPos = transform.position;
		
		computerNormalCollider = GameObject.Find("CaseSnap").GetComponent<MeshCollider>();
		
		foreach(Transform child in transform)
        {
            Trigger tmp = child.GetComponent<Trigger>();
            if (tmp != null)
            {
                tmp.parentComponent = this;
                subTriggers.Add(tmp);
            }
		}

		Collider[] c = GetComponents<Collider>();
		if(c.Length == 2)
		{
			hoverExtraCollider = c[1] as BoxCollider;
		}
		
		if(hoverExtraCollider != null)
			hoverExtraCollider.enabled = false;
	}
	
	
	void Update()
	{
        if (GameHandler.instance.finished)
            return;

		if(dragging)
		{
			Vector3 tmp = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2));
			targetPos = tmp;
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			RaycastHit hit;
			
			bool skip = false;
			
			for(int i = 0; i < Trigger.triggers.Count; i++)
			{
                if (!ContainsTrigger(Trigger.triggers[i]) && Trigger.triggers[i].computerComponent == null && Trigger.triggers[i].collider.Raycast(ray, out hit, 100f))
				{

                    Transform g = GetSlotTransform(Trigger.triggers[i].slotType);
                    if (g != null)
                    {
                        rotation = g.localRotation * Trigger.triggers[i].transform.rotation;
                        targetPos = hit.transform.position - (rotation * g.localPosition);
                    }
                    else
                    {
                        targetPos = hit.transform.position;
                        rotation = Trigger.triggers[i].transform.rotation;
                    }
                    skip = true;
					break;
				}
			}
			
			if(skip == false && computerNormalCollider.Raycast(ray, out hit, 100f))
			{
				if(hit.transform.name == "CaseSnap")
				{
					targetPos = hit.point;
				}
			}
		}

		if (isHovering && !mouseDown && _trigger == null) 
		{
			hoverTime += Time.deltaTime;
			
			Vector3 tmp = startPos;
			tmp.y += 0.05f + Mathf.Abs(Mathf.Sin(hoverTime*4) * 0.1f);
			
			//transform.position = new Vector3(startPos.x, tmp.y, startPos.z);
			targetPos = tmp;
		}
		
		if(_trigger == null || ComputerComponent.isEnabled == false)
		{
            /*
			foreach(Transform child in transform)
			{
				child.gameObject.renderer.enabled = false;
				child.gameObject.collider.enabled = false;
			}
            */

            for (int i = 0; i < subTriggers.Count; i++)
            {
                subTriggers[i].gameObject.renderer.enabled = false;
                subTriggers[i].gameObject.collider.enabled = false;
            }
		}
		else
		{
            /*
			foreach(Transform child in transform)
			{
				Trigger t = child.GetComponent<Trigger>();
				if(t.computerComponent == null)
				{
					child.gameObject.renderer.enabled = true;
					child.gameObject.collider.enabled = true;
				}
			}
            */

            for (int i = 0; i < subTriggers.Count; i++)
            {
                subTriggers[i].gameObject.renderer.enabled = true;
                subTriggers[i].gameObject.collider.enabled = true;
            }
		}

        if (trigger != null)
        {
            //targetPos = trigger.transform.position;

            Transform g = GetSlotTransform(trigger.slotType);
            if (g != null)
            {
                rotation = g.localRotation * trigger.transform.rotation;
                targetPos = trigger.transform.position - (rotation * g.localPosition);
            }
            else
            {
                targetPos = trigger.transform.position;
                rotation = trigger.transform.rotation;
            }
        }
        
		Vector3 tmp2 = transform.position;
		tmp2 += (targetPos - tmp2) * Time.deltaTime*16;
		transform.position = tmp2;

		transform.rotation = Quaternion.Lerp(transform.rotation,rotation,Time.deltaTime * rotationSpeed);
	}
	
	void OnMouseDown()
    {
        if (GameHandler.instance.finished)
            return;

		for(int i = 0; i < subTriggers.Count; i++)
		{
			if(subTriggers[i].computerComponent != null)
			{
				return;
			}
		}
		
		mouseDown = true;
		_lastTrigger = trigger;
		trigger = null;
		hoverExtraCollider.enabled = false;
	}
	
	void OnMouseExit()
    {
        if (GameHandler.instance.finished)
            return;

		if(!dragging)
		{
			if(mouseDown)
			{
				dragging = true;
				isEnabled = true;
			}
			else
			{
				if(hoverObject == this)
					hoverObject = null;
			}
			
			isHovering = false;
			
			hoverTime = 0;
			
			if(_trigger == null && !dragging && !mouseDown)
			{
				targetPos = startPos;
			}
			
			hoverExtraCollider.enabled = false;
		}
		else
		{
		
		}
	}
	
	void OnMouseUp()
    {
        if (GameHandler.instance.finished)
            return;

		hoverObject = null;
		hoverExtraCollider.enabled = false;
	
		for(int i = 0; i < subTriggers.Count; i++)
		{
			if(subTriggers[i].computerComponent != null)
			{
				return;
			}
		}
	
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		RaycastHit hit;
		
		bool triggerFound = false;
		
		for(int i = 0; i < Trigger.triggers.Count; i++)
		{
			if(Trigger.triggers[i].computerComponent == null && Trigger.triggers[i].collider.Raycast(ray, out hit, 100f))
			{
				//transform.position = hit.transform.position;
				trigger = Trigger.triggers[i];
				triggerFound = true;
				break;
			}
		}
		
		if(!dragging)
		{
			trigger = _lastTrigger;
			_lastTrigger = null;
		}
		
		dragging = false;
		mouseDown = false;
		isEnabled = false;
		
		if(!triggerFound && _trigger == null){
			rotation = startRot;
			targetPos = startPos;
		}	

		if(_trigger != null && transform.childCount != 0)
		{
            /*
			foreach(Transform child in transform)
			{
				child.gameObject.renderer.enabled = true;
				child.gameObject.collider.enabled = true;
			}
            */
            for(int i = 0; i < subTriggers.Count; i++)
            {
                subTriggers[i].gameObject.renderer.enabled = true;
                subTriggers[i].gameObject.collider.enabled = true;
            }
		}
	}
	
	void OnMouseOver()
    {
        if (GameHandler.instance.finished)
            return;

		//print(hoverObject);
		if(hoverObject != null)
			return;
		
		hoverObject = this;
		
		if(dragging == false && _trigger == null)
		{
			isHovering = true;
			hoverExtraCollider.enabled = true;

			/*
			Vector3 hover = transform.position + new Vector3(0,Random.Range(0.8f,1.4f),0);
			
			if(hover.y - 1f > startPos.y) hover.y -= 0.5f;
			
			transform.position = Vector3.Lerp(transform.position,hover,Time.deltaTime * 0.5f);
			*/

			componentName = gameObject.name;
			
		}
	}
	
	bool ContainsTrigger(Trigger doesContainTrigger)
	{
		for(int i = 0; i < subTriggers.Count; i++)
		{
			if(subTriggers[i] == doesContainTrigger) return true;		
		}
		return false;
	}

    private Transform GetSlotTransform(Trigger.SlotTypes slotType)
    {
        return transform.FindChild("SlotTransform_" + slotType.ToString());
    }

    private void OnDestroy()
    {
        components.Remove(this);
    }
}
