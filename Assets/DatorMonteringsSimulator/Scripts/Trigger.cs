﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trigger : MonoBehaviour {

    public enum SlotTypes
    {
        None,
        PSU,
        Motherboard,
        HDD,
        PCI,
        RAM,
        CPU,
        Cooler,
        CoolerFan,
        FakeFan
    }

	public static List<Trigger> triggers =  new List<Trigger>();

    public SlotTypes slotType = SlotTypes.None;

	public Quaternion triggerRotation;
	
	private ComputerComponent _computerComponent;
	public ComputerComponent computerComponent
	{
		get
		{
			return _computerComponent;
		}
		set
		{
			_computerComponent = value;
			
			if(_computerComponent == null) collider.enabled = true;
			else collider.enabled = false;
		}
	}

    public ComputerComponent parentComponent;
	
	void Start ()
	{
		computerComponent = null;
		gameObject.renderer.enabled = false;
		triggerRotation = transform.rotation;
		triggers.Add (this);
	}
	
	void LateUpdate()
	{
        if (parentComponent == null)
        {
            if (ComputerComponent.isEnabled && _computerComponent == null) gameObject.renderer.enabled = true;
            else gameObject.renderer.enabled = false;
        }
        
        if(computerComponent != null)
            gameObject.renderer.enabled = false;
	}
	
	void OnDestroy()
	{
		triggers.Remove(this);
	}
}
