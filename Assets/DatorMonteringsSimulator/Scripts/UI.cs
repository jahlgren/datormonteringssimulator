﻿using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour {

    public static UI instance;

	Vector2 textPos;

    public Color errorColor = Color.red;

    public float errorTime = 0;

	void Start()
	{
        instance = this;
		textPos = new Vector2(Input.mousePosition.x, (Screen.height - Input.mousePosition.y)); 
	}
	
	void OnGUI()
	{
		textPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y); 
		
		if(ComputerComponent.hoverObject != null && !ComputerComponent.hoverObject.dragging)
		{
			GUIStyle style = GUI.skin.GetStyle("label");
			style.fontSize = 20;
			style.fontStyle = FontStyle.Bold; 
			style.alignment = TextAnchor.UpperCenter;

            GUI.contentColor = Color.black;
            GUI.Label(new Rect(textPos.x - 102, textPos.y - 28, 200, 30), ComputerComponent.hoverObject.name, style);

            GUI.Label(new Rect(textPos.x - 102, textPos.y - 31, 200, 30), ComputerComponent.hoverObject.name, style);

            GUI.Label(new Rect(textPos.x - 103, textPos.y - 30, 200, 30), ComputerComponent.hoverObject.name, style);

            GUI.Label(new Rect(textPos.x - 101, textPos.y - 30, 200, 30), ComputerComponent.hoverObject.name, style);

			GUI.contentColor = Color.white;
			GUI.Label(new Rect(textPos.x - 102,textPos.y - 30, 200,30), ComputerComponent.hoverObject.name, style);
		}

        if (GameHandler.instance.finished)
        {
            errorTime = 0;

            GUIStyle style2 = GUI.skin.GetStyle("label");
            style2.fontSize = 20;
            style2.fontStyle = FontStyle.Bold;
            style2.alignment = TextAnchor.UpperLeft;

            GUI.contentColor = Color.green;
            GUI.Label(new Rect(10, 10, 400, 30), "Grattis! Ni klarade uppgiften!", style2);
        }

        if (errorTime > 0)
        {
            GUIStyle style2 = GUI.skin.GetStyle("label");
            style2.fontSize = 20;
            style2.fontStyle = FontStyle.Bold;
            style2.alignment = TextAnchor.UpperLeft;

            errorTime -= Time.deltaTime;

            if (errorTime <= 0)
                errorTime = 0;

            if (errorTime < 1)
                errorColor = new Color(errorColor.r, errorColor.g, errorColor.b, errorTime);

            GUI.contentColor = errorColor;
            GUI.Label(new Rect(10, 60, 400, 30), "Något är fel! Försök igen..", style2);
        }
	}
}
