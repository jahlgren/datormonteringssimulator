using UnityEngine;

public class Button2D : MonoBehaviour
{
	public SpriteRenderer spriteRenderer;

	public Sprite defaultSprite;
	public Sprite hoverSprite;

	private Vector2 defaultScale;
	public float hitScale = 1.05f;

	private bool isDown = false;

	protected virtual void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		defaultScale = transform.localScale;
	}
	
	protected virtual void Update()
	{
		Vector2 scale = defaultScale;
	
		Vector2 tmp = new Vector2(Input.mousePosition.x - Screen.width/2, Input.mousePosition.y - Screen.height/2);
		
		if(OverlapPoint(tmp))
		{
			spriteRenderer.sprite = hoverSprite;
			if(Input.GetMouseButtonDown(0))
			{
				isDown = true;
                OnPress();
			}
			if(Input.GetMouseButton(0) && isDown)
				scale = defaultScale * hitScale;
			if(Input.GetMouseButtonUp(0) && isDown)
				OnRelease();
		}
		else
		{
			spriteRenderer.sprite = defaultSprite;
		}
		
		transform.localScale = scale;
		
		if(Input.GetMouseButtonUp(0))
			isDown = false;
	}

    protected virtual void OnPress()
    {

    }
	
	protected virtual void OnRelease()
	{
		
	}
	
	public bool OverlapPoint(Vector2 point)
	{
		Collider2D[] colliders = GetComponents<Collider2D>();
		for(int i = 0; i < colliders.Length; i++)
		{
			if(colliders[i] is BoxCollider2D)
			{
				if((colliders[i] as BoxCollider2D).OverlapPoint(point))
					return true;
			}
		}
		
		return false;
	}
}

