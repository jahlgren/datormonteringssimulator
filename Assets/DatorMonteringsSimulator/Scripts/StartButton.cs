﻿using UnityEngine;
using System.Collections;

public class StartButton : Button2D {


	private void LateUpdate()
	{
		transform.position = new Vector3(-Screen.width/2 + 91 + 10, Screen.height/2 - 20 - 10);
	}

    protected override void  OnPress()
	{
        int correct = 0;

        for (int i = 0; i < ComputerComponent.components.Count; i++)
        {
            if (ComputerComponent.components[i].trigger != null &&
               ComputerComponent.components[i].targetSlotType == ComputerComponent.components[i].trigger.slotType)
            {
                correct++;
            }
        }

        if (correct >= ComputerComponent.components.Count)
        {
            GameHandler.instance.finished = true;
            Destroy(gameObject);
        }
        else
        {
            UI.instance.errorColor = Color.red;
            UI.instance.errorTime = 10;
        }
	}
	
}
