﻿using UnityEngine;
using System.Collections;

public class GameHandler : MonoBehaviour {

    public static GameHandler instance;

    public bool finished = false;

    public SpriteRenderer screenSpriteRenderer;

    private void Awake()
    {
        instance = this;
        Time.maximumDeltaTime = 1f / 30f;
        screenSpriteRenderer.color = new Color(1, 1, 1, 0);
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel("Komponenter");
        }
    	if(Input.GetKeyDown(KeyCode.Escape))
    		Application.Quit();

        if (finished)
        {
            if(screenSpriteRenderer.color.a < 1)
            {
                screenSpriteRenderer.color = new Color(1, 1, 1, screenSpriteRenderer.color.a + Time.deltaTime * 0.5f);
                if (screenSpriteRenderer.color.a >= 1)
                    screenSpriteRenderer.color = Color.white;
            }
        }
    }

}
